<?php

namespace App\Events;

use App\Models\Balance;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserTopup
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $balance;
    public $amount;
    public $subject;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Balance $balance, int $amount)
    {
        $this->user = $user;
        $this->balance = $balance;
        $this->amount = $amount;
        $this->subject = 'user success topup';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
