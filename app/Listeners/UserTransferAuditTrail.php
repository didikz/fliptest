<?php

namespace App\Listeners;

use App\Events\UserTransfered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserTransferAuditTrail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserTransfered  $event
     * @return void
     */
    public function handle(UserTransfered $event)
    {
        activity('transfer')
            ->causedBy($event->user)
            ->performedOn($event->transfer)
            ->withProperties([
                'receiver' => $event->receiver->toArray(),
                'amount' => $event->amount
            ])
            ->log('transfer success');
    }
}
