<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    use HasFactory;

    protected $table = 'transfers';

    protected $fillable = [
        'transfer_id',
        'sender_user_id',
        'receiver_user_id',
        'amount',
        'remark'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_user_id', 'id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_user_id', 'id');
    }
}
