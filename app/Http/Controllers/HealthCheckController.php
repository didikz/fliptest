<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HealthCheckController extends Controller
{
    public function __invoke()
    {
        return response()->json(['status' => 'ok']);
    }
}
