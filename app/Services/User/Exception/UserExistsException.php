<?php

namespace App\Services\User\Exception;

use Exception;

class UserExistsException extends Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return response()->json([
            'message' => 'Username already exists'
        ], 409);
    }
}
