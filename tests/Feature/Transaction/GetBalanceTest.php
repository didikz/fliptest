<?php

namespace Tests\Feature\Transaction;

use App\Models\User;
use App\Services\User\CreateAuthToken;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetBalanceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_get_current_balance()
    {
        $user = User::factory()->create(['current_balance' => 100000]);
        $token = (new CreateAuthToken())->generate($user);
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->get('/api/v1/balance_read');

        $response->assertOk()->assertJson(['balance' => 100000]);
    }

    public function test_user_unable_get_current_balance_with_invalid_token()
    {
        $response = $this->withHeaders(['Authorization' => 'Bearer invalidtoken'])->get('/api/v1/balance_read');
        $response->assertUnauthorized();
    }

    public function test_unauthenticated_user_unable_get_current_balance()
    {
        $response = $this->get('/api/v1/balance_read');
        $response->assertUnauthorized();
    }
}
