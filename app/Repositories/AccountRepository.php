<?php

namespace App\Repositories;

use App\Models\Transfer;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class AccountRepository
{
    public function getTopTransactionByUser(User $user, int $limit = 10)
    {
        return Transfer::select(['id', 'sender_user_id', 'receiver_user_id', 'amount'])
            ->with(['sender:id,username', 'receiver:id,username'])
            ->where('sender_user_id', $user->id)
            ->orWhere('receiver_user_id', $user->id)
            ->orderByDesc('amount')
            ->limit($limit)
            ->get();
    }

    public function getTopUsers(int $limit = 10)
    {
        return Transfer::select([
                    DB::raw('SUM(amount) as transacted_value'),
                    'sender_user_id'
                ])
                ->with(['sender:id,username'])
                ->groupBy('sender_user_id')
                ->orderByDesc('transacted_value')
                ->limit($limit)
                ->get();
    }
}
