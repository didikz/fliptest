<?php

namespace Database\Seeders;

use App\Models\User;
use App\Services\Account\Topup;
use App\Services\Account\Transfer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $amountCollection = [1000, 2000, 5000, 10000];
        $users = User::factory()->count(20)->create();

        foreach ($users as $key => $user) {
            // topups
            (new Topup())->user($user)->amount(900000);

            // make transfer to random user
            $rand = random_int(5, 20);
            for ($i = 1; $i < $rand; $i++) {
                $indexUser = random_int(0, 19);
                if($indexUser != $key) {
                    (new Transfer())->from($user)->to($users[$indexUser]->username)->amount($amountCollection[random_int(0, 3)])->send();
                }
            }
        }
    }
}
