<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    public const TOPUP_LIMIT = 1000000;
    public const TYPE_CREDIT = 'credit';
    public const TYPE_DEBIT = 'debit';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'amount',
        'type',
        'remark'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    public static function isAllowedToTopup(int $amount)
    {
        return $amount < self::TOPUP_LIMIT;
    }
}
