<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TopTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if ($request->user()->id == $this->sender_user_id) {
            $username = $this->receiver->username;
            $amount = (int) $this->amount * -1;
        } else {
            $username = $this->sender->username;
            $amount = (int) $this->amount;
        }
        return [
            'username' => $username,
            'amount' => $amount
        ];
    }
}
