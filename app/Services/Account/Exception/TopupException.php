<?php

namespace App\Services\Account\Exception;

use Exception;

class TopupException extends Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json([
            'message' => 'Invalid topup amount'
        ], 400);
    }
}
