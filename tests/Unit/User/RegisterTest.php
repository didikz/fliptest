<?php

namespace Tests\Unit\User;

use Tests\TestCase;
use App\Models\User;
use App\Services\User\Register;
use App\Services\User\CreateAuthToken;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_create_user_and_token_is_generated()
    {
        $register = new Register(new CreateAuthToken());
        $register->create('johndoe');
        $this->assertEquals($register->user()->username, 'johndoe');
        $this->assertTrue(!empty($register->getAuthToken()));
    }

    public function test_user_can_generate_auth_token()
    {
        $user = User::factory()->make(['username' => 'janedoe']);
        $createAuthToken = new CreateAuthToken();
        $this->assertTrue(!empty($createAuthToken->generate($user)));
    }
}
