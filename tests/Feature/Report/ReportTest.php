<?php

namespace Tests\Feature\Report;

use App\Models\User;
use App\Services\Account\Transfer;
use App\Services\User\CreateAuthToken;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReportTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_empty_top_transaction_by_user()
    {
        $user = User::factory()->create(['current_balance' => 100000]);
        $token = (new CreateAuthToken())->generate($user);
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->get('/api/v1/top_transactions_per_user');
        $response->assertOk()->assertJson([]);
    }

    public function test_top_transaction_by_user()
    {
        $user = User::factory()->create(['current_balance' => 100000]);
        $receiver = User::factory()->create(['current_balance' => 100000]);
        $token = (new CreateAuthToken())->generate($user);
        for ($i = 0; $i < 3; $i++) {
            (new Transfer())->from($user)->to($receiver->username)->amount(random_int(1000, 2000))->send();
        }

        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->get('/api/v1/top_transactions_per_user');
        $response->assertOk()->assertJsonCount(3);
    }

    public function test_unauthenticated_top_transaction_by_user()
    {
        $response = $this->get('/api/v1/top_transactions_per_user');
        $response->assertUnauthorized();
    }

    public function test_empty_top_users()
    {
        $user = User::factory()->create(['current_balance' => 100000]);
        $token = (new CreateAuthToken())->generate($user);
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->get('/api/v1/top_users');
        $response->assertOk()->assertJson([]);
    }

    public function test_top_users()
    {
        $user = User::factory()->create(['current_balance' => 100000]);
        $receiver = User::factory()->create(['current_balance' => 100000]);
        $token = (new CreateAuthToken())->generate($user);
        for ($i = 0; $i < 3; $i++) {
            (new Transfer())->from($user)->to($receiver->username)->amount(random_int(1000, 2000))->send();
        }

        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])->get('/api/v1/top_users');
        $response->assertOk()->assertJsonCount(1);
    }

    public function test_unauthenticated_top_users()
    {
        $response = $this->get('/api/v1/top_users');
        $response->assertUnauthorized();
    }
}
