<?php

namespace App\Services\User\Exception;

class InvalidTokenException extends \Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return response()->json([
            'message' => 'User token not valid'
        ], 401);
    }
}
