<?php

namespace Tests\Feature\Transaction;

use App\Models\Balance;
use App\Models\User;
use App\Services\User\CreateAuthToken;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTransferTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_transfer_to_valid_receiver_with_valid_amount()
    {
        $sender = User::factory()->create(['current_balance' => 1000]);
        $receiver = User::factory()->create(['current_balance' => 500]);
        $token = (new CreateAuthToken())->generate($sender);

        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])
                        ->post('/api/v1/transfer', ['to_username' => $receiver->username, 'amount' => 500]);

        $response->assertNoContent();

        $this->assertDatabaseHas('users', ['id' => $sender->id, 'current_balance' => 500]);
        $this->assertDatabaseHas('balances', ['user_id' => $sender->id, 'amount' => 500, 'type' => Balance::TYPE_DEBIT, 'remark' => 'transfer to ' . $receiver->username]);
        $this->assertDatabaseHas('transfers', ['sender_user_id' => $sender->id, 'receiver_user_id' => $receiver->id, 'amount' => 500, 'remark' => 'transfer to ' . $receiver->username]);

        $this->assertDatabaseHas('users', ['id' => $receiver->id, 'current_balance' => 1000]);
        $this->assertDatabaseHas('balances', ['user_id' => $receiver->id, 'amount' => 500, 'type' => Balance::TYPE_CREDIT, 'remark' => 'received from ' . $sender->username]);

        $this->assertDatabaseHas('activity_log', ['description' => 'transfer success']);

    }

    public function test_user_transfer_to_valid_receiver_with_insufficient_fund_balance()
    {
        $sender = User::factory()->create(['current_balance' => 1000]);
        $receiver = User::factory()->create(['current_balance' => 500]);
        $token = (new CreateAuthToken())->generate($sender);

        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])
                        ->post('/api/v1/transfer', ['to_username' => $receiver->username, 'amount' => 1500]);

        $response->assertStatus(400)->assertJson(['message' => 'Insufficient balance']);
        $this->assertDatabaseHas('users', ['id' => $sender->id, 'current_balance' => 1000]);
        $this->assertDatabaseHas('users', ['id' => $receiver->id, 'current_balance' => 500]);
    }

    public function test_user_transfer_to_valid_receiver_with_invalid_amount()
    {
        $sender = User::factory()->create(['current_balance' => 1000]);
        $receiver = User::factory()->create(['current_balance' => 500]);
        $token = (new CreateAuthToken())->generate($sender);

        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])
                        ->post('/api/v1/transfer', ['to_username' => $receiver->username, 'amount' => '']);

        $response->assertStatus(422);
        $this->assertDatabaseHas('users', ['id' => $sender->id, 'current_balance' => 1000]);
        $this->assertDatabaseHas('users', ['id' => $receiver->id, 'current_balance' => 500]);
    }

    public function test_user_transfer_to_invalid_receiver_with_valid_amount()
    {
        $sender = User::factory()->create(['current_balance' => 1000]);
        $token = (new CreateAuthToken())->generate($sender);

        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])
                        ->post('/api/v1/transfer', ['to_username' => 'janedoe', 'amount' => 500]);

        $response->assertStatus(404)->assertJson(['message' => 'Destination user not found']);
        $this->assertDatabaseHas('users', ['id' => $sender->id, 'current_balance' => 1000]);
    }

    public function test_user_unable_transfer_using_invalid_token()
    {
        $receiver = User::factory()->create(['current_balance' => 500]);

        $response = $this->withHeaders(['Authorization' => 'Bearer invalidtoken'])
                        ->post('/api/v1/transfer', ['to_username' => $receiver->username, 'amount' => 500]);
        $response->assertUnauthorized();
    }

    public function test_unauthenticated_user_unable_transfer()
    {
        $response = $this->post('/api/v1/transfer', ['to_username' => 'johndoe', 'amount' => 500]);
        $response->assertUnauthorized();
    }
}
