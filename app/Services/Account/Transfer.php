<?php

namespace App\Services\Account;

use App\Events\UserTransfered;
use App\Models\Balance;
use App\Models\User;
use App\Models\Transfer as TransferModel;
use App\Services\Account\Exception\TransferException;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class Transfer
{
    protected User $sender;
    protected User $receiver;
    protected string $receiverUsername;
    protected int $amount;

    public function from(User $sender): Transfer
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @throws TransferException
     */
    public function to(string $username): Transfer
    {
        if ($receiver = User::where('username', $username)->first()) {
            $this->receiver = $receiver;
            return $this;
        }
        throw new TransferException('Destination user not found', 404);
    }

    /**
     * @throws TransferException
     */
    public function amount(int $amount): Transfer
    {
        if ($this->isBalanceSufficient($this->sender, $amount)) {
            $this->amount = $amount;
            return $this;
        }
        throw new TransferException('Insufficient balance', 400);
    }

    /**
     * @throws \Exception
     */
    public function send()
    {
        DB::beginTransaction();
        try {
            // make transfer
            $this->sender->transfers()->save(new TransferModel([
                'transfer_id' => Uuid::uuid4(),
                'receiver_user_id' => $this->receiver->id,
                'amount' => $this->amount,
                'remark' => 'transfer to ' . $this->receiver->username
            ]));

            // create debit balance sender
            $this->sender->balances()->save(new Balance(['amount' => $this->amount, 'type' => Balance::TYPE_DEBIT, 'remark' => 'transfer to ' . $this->receiver->username]));
            $this->sender->deductCurrentBalance($this->amount);
            $this->sender->save();

            // create credit balance receiver
            $this->receiver->balances()->save(new Balance(['amount' => $this->amount, 'type' => Balance::TYPE_CREDIT, 'remark' => 'received from ' . $this->sender->username]));
            $this->receiver->addCurrentBalance($this->amount);
            $this->receiver->save();
            DB::commit();

            UserTransfered::dispatch($this->sender, $this->receiver, $this->sender->transfers()->latest()->first(), $this->amount);
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \Exception('Something went wrong during transfer. Please try again later. Error: '. $exception->getMessage());
        }
    }

    protected function isBalanceSufficient(User $sender, int $amount): bool
    {
        return $sender->current_balance >= $amount;
    }
}
