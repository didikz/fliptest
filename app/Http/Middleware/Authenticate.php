<?php

namespace App\Http\Middleware;

use App\Services\User\Exception\InvalidTokenException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Tymon\JWTAuth\Exceptions\JWTException;

class Authenticate extends Middleware
{
    protected function unauthenticated($request, array $guards)
    {
        throw new InvalidTokenException();
    }
}
