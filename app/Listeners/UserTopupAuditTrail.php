<?php

namespace App\Listeners;

use App\Events\UserTopup;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserTopupAuditTrail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserTopup  $event
     * @return void
     */
    public function handle(UserTopup $event)
    {
        activity('topup')->causedBy($event->user)->performedOn($event->balance)->withProperties(['amount' => $event->amount])->log($event->subject);
    }
}
