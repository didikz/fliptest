<?php

namespace Tests\Feature\Transaction;

use Tests\TestCase;
use App\Models\User;
use App\Services\User\CreateAuthToken;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTopupTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_able_to_topup_with_valid_amount()
    {
        $user = User::factory()->create();
        $token = (new CreateAuthToken())->generate($user);
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])
                        ->post('/api/v1/balance_topup', ['amount' => 100000]);
        $response->assertNoContent();
        $this->assertDatabaseHas('balances', ['user_id' => $user->id, 'amount' => 100000, 'type' => 'credit']);
        $this->assertDatabaseCount('balances', 1);
        $this->assertDatabaseHas('users', ['id' => $user->id, 'current_balance' => 100000]);

        // do more topup
        $response2 = $this->withHeaders(['Authorization' => 'Bearer ' . $token])
                        ->post('/api/v1/balance_topup', ['amount' => 50000]);
        $response2->assertNoContent();
        $this->assertDatabaseHas('balances', ['user_id' => $user->id, 'amount' => 50000, 'type' => 'credit']);
        $this->assertDatabaseCount('balances', 2);
        $this->assertDatabaseHas('users', ['id' => $user->id, 'current_balance' => 150000]);

        $this->assertDatabaseHas('activity_log', ['description' => 'user success topup']);
    }

    public function test_user_unable_to_topup_with_invalid_amount()
    {
        $user = User::factory()->create();
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . (new CreateAuthToken())->generate($user)])
                    ->post('/api/v1/balance_topup', ['amount' => 'invalid']);
        $response->assertStatus(400)->assertJson(['message' => 'Invalid topup amount']);
        $this->assertDatabaseMissing('balances', ['user_id' => $user->id, 'amount' => 100000, 'type' => 'credit']);
        $this->assertDatabaseCount('balances', 0);
        $this->assertDatabaseMissing('users', ['id' => $user->id, 'current_balance' => 100000]);
    }

    public function test_user_unable_to_topup_with_above_amount_limit()
    {
        $user = User::factory()->create();
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . (new CreateAuthToken())->generate($user)])
                        ->post('/api/v1/balance_topup', ['amount' => 20000000]);
        $response->assertStatus(400)->assertJson(['message' => 'Invalid topup amount']);
        $this->assertDatabaseMissing('balances', ['user_id' => $user->id, 'amount' => 20000000, 'type' => 'credit']);
        $this->assertDatabaseCount('balances', 0);
        $this->assertDatabaseMissing('users', ['id' => $user->id, 'current_balance' => 20000000]);
    }

    public function test_user_unable_to_topup_with_invalid_token()
    {
        $response = $this->withHeaders(['Authorization' => 'Bearer invalidtoken'])
                        ->post('/api/v1/balance_topup', ['amount' => 1000000]);
        $response->assertStatus(401)->assertJson(['message' => 'User token not valid']);
    }

    public function test_unauthenticated_user_unable_to_topup()
    {
        $response = $this->post('/api/v1/balance_topup', ['amount' => 1000000]);
        $response->assertStatus(401)->assertJson(['message' => 'User token not valid']);
    }
}
