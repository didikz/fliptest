## Simple E-Wallet Backend Platform

This simple E-wallet backend API platform will cover basic functionalities, such are:

1. Registration
2. Topup
3. Transfer
4. Get balance
5. Reporting - Top transactions
6. Reporting - Top users

Built with current stacks:
1. Laravel PHP Framework
2. SQlite
3. JWT

### Design Architecture Basic Idea

We want to cover architecture that simple but yet advanced enough with focusing on separation of concern and making it testable as many as possible
+ `Services` namespace: Handling application logic, and will be separate by domain or concerns
+ `Models` namespace: Held all models definition
+ `Repositories` namespace: Handling data collection that can be used as reporting data or other data collection
+ `Resources` namespace: Transforming resource or collection for encapsulate presenter data logic and making it consistence

> Each service as registration, topup, and transfer will dispatch an activity log or audit trail data for tracking activity purposes.

### Installation / Local deployment

1. Navigate to project root and run `composer install` to install dependencies
2. Setup `.env` file by copying `.env.example`
   1. Generate application key with command `php artisan key:generate`
   2. Generate JWT Secret key with command `php artisan jwt:secret`
3. Create `database.sqlite` file as our database inside `database` directory
4. Run migration `php artisan migrate` and data seeder to populate some dummy data `php artisan db:seed`
5. Finally, run API through your favourite REST API client with command `php artisan serve`

### Authentication

Except registration API, all endpoints is secured by authentication token using [JWT](https://jwt.io/). To access protected endpoints you have to add `Authorization: Bearer <jwt-token>` in request header.

For generating token as testing purposes you may able to run using command:

```bash
php artisan token:generate {shopId}
```

It will give you JWT token nicely.

### Tests

Run E2E & unit tests with command

```bash
php artisan test --env=testing
```
