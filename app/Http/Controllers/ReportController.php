<?php

namespace App\Http\Controllers;

use App\Http\Resources\TopTransactionResource;
use App\Http\Resources\TopUserResource;
use App\Models\Transfer;
use App\Repositories\AccountRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    protected $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function topTransaction(Request $request)
    {
        return TopTransactionResource::collection($this->accountRepository->getTopTransactionByUser($request->user('api')));
    }

    public function topUser(Request $request)
    {
        return TopUserResource::collection($this->accountRepository->getTopUsers());
    }
}
