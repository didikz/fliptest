<?php

namespace App\Services\User;

use App\Models\User;

class CreateAuthToken
{
    public function generate(User $user): ?string
    {
        return auth()->login($user);
    }
}
