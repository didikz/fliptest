<?php

namespace App\Services\Account;

use App\Events\UserTopup;
use App\Models\User;
use App\Models\Balance;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Services\Account\Exception\TopupException;

class Topup
{
    protected User $user;
    protected int $amount;

    public function user(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function amount(int $amount)
    {
        if (!Balance::isAllowedToTopup($amount)) {
            throw new TopupException();
        }

        DB::beginTransaction();
        try {
            $this->user->balances()->save(new Balance(['amount' => $amount, 'type' => Balance::TYPE_CREDIT]));
            $this->user->addCurrentBalance($amount);
            $this->user->save();
            DB::commit();

            UserTopup::dispatch($this->user, $this->user->balances()->latest()->first(), $amount);

        } catch (Exception $e) {
            DB::rollback();
            throw new Exception('Something went wrong during topup. Please try again later.');
        }
    }
}
