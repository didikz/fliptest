<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_register_with_nonexist_user()
    {
        $username = 'johndoe' . mt_rand(1, 100);
        $response = $this->post('/api/v1/create_user', ['username' => $username]);

        $response->assertStatus(200)->assertJsonStructure(['token']);
        $this->assertDatabaseHas('users', ['username' => $username]);
        $this->assertDatabaseHas('activity_log', ['description' => 'registration']);
    }

    public function test_register_with_exists_user()
    {
        $user = User::factory()->create(['username' => 'janedoe']);
        $response = $this->post('/api/v1/create_user', ['username' => 'janedoe']);
        $response->assertStatus(409)->assertJson(['message' => 'Username already exists']);
    }

    public function test_register_with_invalid_parameter()
    {
        $response = $this->post('/api/v1/create_user', ['username' => '']);
        $response->assertStatus(400)->assertJson(['message' => 'Bad request']);
    }
}
