<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HealthCheckTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_healthcheck_api_is_valid()
    {
        $response = $this->get('/');

        $response->assertStatus(200)->assertJson(['status' => 'ok']);
    }
}
