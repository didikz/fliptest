<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\User\Register;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected Register $register;

    public function __construct(Register $register)
    {
        $this->register = $register;
    }
    
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Bad request'], 400);
        }

        $token = $this->register->create($request->username)->getAuthToken();
        return response()->json(['token' => $token]);
    }
}
