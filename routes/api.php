<?php

use App\Http\Controllers\AccountBalanceController;
use App\Http\Controllers\ReportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TopupController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('/create_user', [UserController::class, 'register'])->name('register');
    Route::middleware('auth:api')->group(function () {
        Route::post('/balance_topup', [TopupController::class, 'topup'])->name('topup');
        Route::get('/balance_read', [AccountBalanceController::class, 'balance'])->name('get-balance');
        Route::post('/transfer', [AccountBalanceController::class, 'transfer'])->name('transfer');
        Route::get('/top_transactions_per_user', [ReportController::class, 'topTransaction'])->name('top-transactions');
        Route::get('/top_users', [ReportController::class, 'topUser'])->name('top-users');
    });
});
