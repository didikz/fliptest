<?php

namespace App\Http\Controllers;

use App\Services\Account\Transfer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AccountBalanceController extends Controller
{
    protected $transferService;

    public function __construct(Transfer $transferService)
    {
        $this->transferService = $transferService;
    }

    public function balance(Request $request)
    {
        return response()->json([
            'balance' => $request->user('api')->current_balance
        ]);
    }

    /**
     * @throws \App\Services\Account\Exception\TransferException
     * @throws ValidationException
     */
    public function transfer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'to_username' => ['required', 'string'],
            'amount' => ['required', 'integer']
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()], 422);
        }

        $this->transferService->from($request->user('api'))
            ->to($request->to_username)
            ->amount($request->amount)
            ->send();

        return response()->json(['message' => 'Transfer success'], 204);
    }
}
