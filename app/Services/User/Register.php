<?php

namespace App\Services\User;

use App\Events\UserRegistered;
use App\Models\User;
use App\Services\User\CreateAuthToken;
use App\Services\User\Exception\UserExistsException;

class Register
{
    protected User $user;
    protected string $token;
    protected CreateAuthToken $userAuthToken;

    public function __construct(CreateAuthToken $userAuthToken)
    {
        $this->userAuthToken = $userAuthToken;
    }

    public function create(string $username): Register
    {
        $this->validateExists($username);

        $this->user = User::create([
            'username' => $username,
            'name' => $username,
            'password' => bcrypt('password'),
        ]);

        $this->token = $this->userAuthToken->generate($this->user);

        UserRegistered::dispatch($this->user);

        return $this;
    }

    public function getAuthToken(): string
    {
        return $this->token;
    }

    public function user(): User
    {
        return $this->user;
    }

    protected function validateExists(string $username)
    {
        if (User::where('username', $username)->exists()) {
            throw new UserExistsException();
        }
    }
}
