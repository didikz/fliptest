<?php

namespace App\Events;

use App\Models\Balance;
use App\Models\Transfer;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserTransfered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $receiver;
    public $transfer;
    public $amount;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        User $receiver,
        Transfer $transfer,
        int $amount
    )
    {
        $this->user = $user;
        $this->receiver = $receiver;
        $this->transfer = $transfer;
        $this->amount = $amount;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
