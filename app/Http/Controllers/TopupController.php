<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Account\Topup;
use Illuminate\Support\Facades\Validator;

class TopupController extends Controller
{
    protected Topup $topup;

    public function __construct(Topup $topup)
    {
        $this->topup = $topup;
    }

    public function topup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => ['required', 'integer'],
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Invalid topup amount'], 400);
        }

        $this->topup->user($request->user())->amount($request->amount);

        return response()->json(['message' => 'Topup successful'], 204);
    }
}
